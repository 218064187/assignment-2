This assignment aims to build an image classifier using a convolutional neural network (CNN). The images, accessible at https://www.kaggle.com/
paultimothymooney/chest-xray-pneumonia, represent chest X-ray images
of patients. Two possible types of labels are considered: normal and pneumonia. Put another way, each sample represents either a normal patient or
a patient suffering from pneumonia. You are required to build your model
following the LeNet 5 architecture presented in class.
You will implement your model in the Julia programming language and use
the Flux package for neural networks.
